package ${packageName}.controller.admin;

import com.xueyi.common.core.web.result.AjaxResult;
import com.xueyi.common.core.web.validate.V_A;
import com.xueyi.common.core.web.validate.V_E;
import com.xueyi.common.log.annotation.Log;
import com.xueyi.common.log.enums.BusinessType;
import org.springframework.security.access.prepost.PreAuthorize;
import ${packageName}.controller.base.B${ClassName}Controller;
import ${packageName}.domain.dto.${ClassName}Dto;
import ${packageName}.domain.query.${ClassName}Query;
import com.xueyi.common.security.annotation.AdminAuth;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

#if($api.export)
import jakarta.servlet.http.HttpServletResponse;
#end
#if($api.getInfo)
import java.io.Serializable;
#end
#if($api.batchRemove)
import java.util.List;
#end

/**
 * ${functionName}管理 | 管理端 业务处理
 *
 * @author ${author}
 */
@AdminAuth
@RestController
@RequestMapping("/admin/${businessName}")
public class A${ClassName}Controller extends B${ClassName}Controller {
#if($api.cache)

    /**
     * 刷新${functionName}缓存
     */
    @Override
#if($isDependMode)
    @PreAuthorize("@ss.hasAuthority(@Auth.${ClASS_NAME}_CACHE)")
#else
    @PreAuthorize("@ss.hasAuthority('${authorityName}:${businessName}:cache')")
#end
    @Log(title = "${functionName}管理", businessType = BusinessType.REFRESH)
    @GetMapping("/refresh")
    public AjaxResult refreshCache() {
        return super.refreshCache();
    }
#end
#if($api.list)

    /**
     * 查询${functionName}列表
     */
    @Override
    @GetMapping("/list")
#if($isDependMode)
    @PreAuthorize("@ss.hasAuthority(@Auth.${ClASS_NAME}_LIST)")
#else
    @PreAuthorize("@ss.hasAuthority('${authorityName}:${businessName}:list')")
#end
    public AjaxResult list(${ClassName}Query ${classNameNoPrefix}) {
        return super.list(${classNameNoPrefix});
    }
#end
#if($api.getInfo)

    /**
     * 查询${functionName}详细
     */
    @Override
    @GetMapping(value = "/{id}")
#if($isDependMode)
    @PreAuthorize("@ss.hasAuthority(@Auth.${ClASS_NAME}_SINGLE)")
#else
    @PreAuthorize("@ss.hasAuthority('${authorityName}:${businessName}:single')")
#end
    public AjaxResult getInfo(@PathVariable Serializable id) {
        return super.getInfo(id);
    }
#end
#if($api.export)

    /**
     * ${functionName}导出
     */
    @Override
    @PostMapping("/export")
#if($isDependMode)
    @PreAuthorize("@ss.hasAuthority(@Auth.${ClASS_NAME}_EXPORT)")
#else
    @PreAuthorize("@ss.hasAuthority('${authorityName}:${businessName}:export')")
#end
    @Log(title = "${functionName}管理", businessType = BusinessType.EXPORT)
    public void export(HttpServletResponse response, ${ClassName}Query ${classNameNoPrefix}) {
        super.export(response, ${classNameNoPrefix});
    }
#end
#if($api.add)

    /**
     * ${functionName}新增
     */
    @Override
    @PostMapping
#if($isDependMode)
    @PreAuthorize("@ss.hasAuthority(@Auth.${ClASS_NAME}_ADD)")
#else
    @PreAuthorize("@ss.hasAuthority('${authorityName}:${businessName}:add')")
#end
    @Log(title = "${functionName}管理", businessType = BusinessType.INSERT)
    public AjaxResult add(@Validated({V_A.class}) @RequestBody ${ClassName}Dto ${classNameNoPrefix}) {
        return super.add(${classNameNoPrefix});
    }
#end
#if($api.edit)

    /**
     * ${functionName}修改
     */
    @Override
    @PutMapping
#if($isDependMode)
    @PreAuthorize("@ss.hasAuthority(@Auth.${ClASS_NAME}_EDIT)")
#else
    @PreAuthorize("@ss.hasAuthority('${authorityName}:${businessName}:edit')")
#end
    @Log(title = "${functionName}管理", businessType = BusinessType.UPDATE)
    public AjaxResult edit(@Validated({V_E.class}) @RequestBody ${ClassName}Dto ${classNameNoPrefix}) {
        return super.edit(${classNameNoPrefix});
    }
#end
#if($api.editStatus)

    /**
     * ${functionName}修改状态
     */
    @Override
    @PutMapping("/status")
#if($isDependMode)
    @PreAuthorize("@ss.hasAnyAuthority(@Auth.${ClASS_NAME}_EDIT, @Auth.${ClASS_NAME}_ES)")
#else
    @PreAuthorize("@ss.hasAnyAuthority('${authorityName}:${businessName}:edit', '${authorityName}:${businessName}:es')")
#end
    @Log(title = "${functionName}管理", businessType = BusinessType.UPDATE_STATUS)
    public AjaxResult editStatus(@RequestBody ${ClassName}Dto ${classNameNoPrefix}) {
        return super.editStatus(${classNameNoPrefix});
    }
#end
#if($api.batchRemove)

    /**
     * ${functionName}批量删除
     */
    @Override
    @DeleteMapping("/batch/{idList}")
#if($isDependMode)
    @PreAuthorize("@ss.hasAuthority(@Auth.${ClASS_NAME}_DEL)")
#else
    @PreAuthorize("@ss.hasAuthority('${authorityName}:${businessName}:delete')")
#end
    @Log(title = "${functionName}管理", businessType = BusinessType.DELETE)
    public AjaxResult batchRemove(@PathVariable List<Long> idList) {
        return super.batchRemove(idList);
    }
#end
#if($isDependMode)

    interface Auth {
#if($api.list)
        /** ${functionName}管理 - 列表 */
        String ${ClASS_NAME}_LIST = "${authorityName}:${businessName}:list";
#end
#if($api.getInfo)
        /** ${functionName}管理 - 详情 */
        String ${ClASS_NAME}_SINGLE = "${authorityName}:${businessName}:single";
#end
#if($api.add)
        /** ${functionName}管理 - 新增 */
        String ${ClASS_NAME}_ADD = "${authorityName}:${businessName}:add";
#end
#if($api.edit)
        /** ${functionName}管理 - 修改 */
        String ${ClASS_NAME}_EDIT = "${authorityName}:${businessName}:edit";
#end
#if($api.editStatus)
        /** ${functionName}管理 - 修改状态 */
        String ${ClASS_NAME}_ES = "${authorityName}:${businessName}:es";
#end
#if($api.batchRemove)
        /** ${functionName}管理 - 删除 */
        String ${ClASS_NAME}_DEL = "${authorityName}:${businessName}:delete";
#end
#if($api.import)
        /** ${functionName}管理 - 导入 */
        String ${ClASS_NAME}_IMPORT = "${authorityName}:${businessName}:import";
#end
#if($api.export)
        /** ${functionName}管理 - 导出 */
        String ${ClASS_NAME}_EXPORT = "${authorityName}:${businessName}:export";
#end
#if($api.cache)
        /** ${functionName}管理 - 缓存 */
        String ${ClASS_NAME}_CACHE = "${authorityName}:${businessName}:cache";
#end
    }
#end
}
